#ifndef _BASIC_UTILS_H_
#define _BASIC_UTILS_H_

#include <type_traits>

namespace lg{
//definitions of different tpyes of numerical values:
//the order is by size, and signifity:
typedef unsigned long long	uint64;
typedef long long			int64;

typedef unsigned int		uint32;
//alias for uint32
typedef uint32				uint;
//alias for int
typedef int					int32;

typedef unsigned short		uint16;
typedef short				int16;
typedef unsigned char		uint8;
typedef char				int8;
};

template <class T>
inline T castToEnum(typename std::underlying_type<T>::type val)
{
	return static_cast<T>(val);
}

template <class T>
inline typename std::underlying_type<T>::type castEnum(T val)
{
	return static_cast<typename std::underlying_type<T>::type>(val);
}

#endif	//_BASIC_UTILS_H_