
#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_ERRORS

#include <iostream>
#include <string>
#include <fstream>

#include "parser.h"

bool isHelpRequest(int size, char** seq)
{
	for(int i = 0; i < size; ++i)
	{
		if (seq[i] == "help" || seq[i] == "-help" || seq[i] == "/help" || seq[i] == "--help")
			return true;
	}
	return false;
}

bool isSet(const std::string& str, const std::vector<std::string>& vec)
{
	for(int i = 0, j = vec.size(); i < j; ++i)
	{
		if (str == vec[i])
		{
			return true;
		}
	}
	return false;
}

std::string getColor(const std::vector<std::string>& vec)
{
	for(int i = 0, j = vec.size(); i < j; ++i)
	{
		std::string str = vec[i];
		if (str.substr(0, 7) == "-color=")
		{
			return str.substr(7);
		}
	}
	return "";
}

std::string getColorIn(const std::vector<std::string>& vec)
{
	for(int i = 0, j = vec.size(); i < j; ++i)
	{
		std::string str = vec[i];
		if (str.substr(0, 9) == "-hkcolor=")
		{
			return str.substr(9);
		}
	}
	return "";
}

std::string getColorLevel(const std::vector<std::string>& vec)
{
	for(int i = 0, j = vec.size(); i < j; ++i)
	{
		std::string str = vec[i];
		if (str.substr(0, 12) == "-levelColor=")
		{
			return str.substr(12);
		}
	}
	return "";
}

std::string getInputFilePath(const std::vector<std::string>& vec)
{
	for(int i = 0, j = vec.size(); i < j; ++i)
	{
		if (vec[i].substr(0, 4) == "-in=" || vec[i].substr(0, 8) == "-infile=")
			return vec[i].substr(4);
	}
	return "";
}

std::vector<std::string> getOutputFilePath(const std::vector<std::string>& vec)
{
	std::vector<std::string> result;

	for(int i = 0, j = vec.size(); i < j; ++i)
	{
		if (vec[i].substr(0, 5) == "-out=")
			result.push_back(vec[i].substr(5));
		else if (vec[i].substr(0, 9) == "outfile=")
			result.push_back(vec[i].substr(9));
	}
	return result;
}

std::string getCompareOutputFilePath(const std::vector<std::string>& vec)
{
	for(int i = 0, j = vec.size(); i < j; ++i)
	{
		if (vec[i].substr(0, 12) == "-compareout=")
			return vec[i].substr(12);
	}
	return "";
}

enum CpuMode{
	None,
	Khz,
	Mhz,
	Ghz
};

CpuMode getCpuClockMode(const std::string& str)
{
	//get the last 3 characters of the string
	std::string newS = inner::upperCase(str.substr(str.size()-3));

	if (newS == inner::upperCase("Khz"))
		return CpuMode::Khz;
	else if (newS == inner::upperCase("Mhz"))
		return CpuMode::Mhz;
	else if (newS == inner::upperCase("Ghz"))
		return CpuMode::Ghz;
	return CpuMode::None;
}

float getCpuClockValue(const std::string& str)
{
	//get the last character
	char last = str.back();

	int backOffset = 0;

	//if it is z or Z
	if (last == 'z' || last == 'Z')
	{
		backOffset++;
		//then there must be symbol before it
		if (str[str.size()-2] != 'H' && str[str.size()-2] != 'h')
			//if there is neither H or h before Z, it is malformed and is therefore 0
			return 0;
		else
		{
			backOffset++;

			//load the character
			char c = str[str.size()-3];

			//if it is either of g, G, m, M, k, K
			if (c == 'g' || c == 'G' || c == 'm' || c == 'M' || c == 'k' || c == 'K')
				//offset last position to read
				backOffset++;
		}
	}

	float result = 0;

	try{
		result = std::stof(str.substr(0, str.size() - backOffset));
	}catch(...) { /* do nothing if we caught something */std::cout << "caught\n"; }

	return result;

}

long long getCpuClock(const std::vector<std::string>& vec)
{
	long long reslt = 0;
	long long multiplier = 0;

	for(int i = 0, j = vec.size(); i < j; ++i)
	{
		if (vec[i].substr(0, 10) == "-cpuclock=")
		{
			//get the mode inserted by user
			auto mode = getCpuClockMode(vec[i].substr(10));

			//properly dispatch it
			switch (mode)
			{
			case CpuMode::Mhz:
				multiplier = 1000000;
				break;
			case CpuMode::Khz:
				multiplier = 1000;
				break;
			case CpuMode::Ghz:
				multiplier = 1000000000;
				break;
			case CpuMode::None:
				multiplier = 1;
				break;
			}

			//get the value
			auto num = getCpuClockValue(vec[i].substr(10));

			reslt = num * multiplier;
		}
	}
	return reslt;
}

KeyboardLayout getKeyboardLayout(const std::vector<std::string>& vec)
{
	for(auto&& a : vec)
	{
		if (a.substr(0, 10) == "-keyboard=")
		{
			std::string s = a.substr(10);

			if (s == "english" || s == "qwertz")
			{
				KeyboardLayout english;
				english.addMapping('Y', 'Z');

				return english;
			}
			else if (s == "french" || s == "azerty")
			{
				KeyboardLayout french;
				french.addMapping('Q', 'A');
				french.addMapping('W', 'Z');
				french.addMapping('A', 'Q');
				french.addMapping('Y', 'W');

				return french;
			}
			else
				//return unmapped one, which is the default for me
				return KeyboardLayout();
		}
	}
	//unmapped keyboard is equal to german layout
	//which is the same I use
	return KeyboardLayout();
}

int main(int argc, char** argv)
{
	/*
	std::string s = "Train Druid of the |cffffcc00T|ralon";
	std::cout << inner::checkRemoveColorCode(s, "ffffcc00") << "\n";

	auto argv1 = argv[0];
	argc = 5;
	std::string in = "-in=CustomKeysSample.txt";
	std::string out = "-out=file.txt";
	std::string hC = "-hkcolor=color";
	std::string cC = "-color=ffffcc00";

	argv = new char*[argc];
	argv[1] = new char[in.size()+1];
	memcpy(argv[1], &in[0], in.size());
	argv[1][in.size()] = '\0';

	argv[2] = new char[out.size()+1];
	memcpy(argv[2], &out[0], out.size());
	argv[2][out.size()] = '\0';
	
	argv[3] = new char[hC.size()+1];
	memcpy(argv[3], &hC[0], hC.size());
	argv[3][hC.size()] = '\0';
	
	argv[4] = new char[cC.size()+1];
	memcpy(argv[4], &cC[0], cC.size());
	argv[4][cC.size()] = '\0';

	argv[0] = argv1;
	*/

	if (argc == 1 || isHelpRequest(argc, argv))
	{
		std::cout << "\n  Syntax:\n";
		std::cout << "\n      -in=path_to_input_file, -infile=path_to_input_file";
		std::cout << "\n          specifies the input file to read from";
		std::cout << "\n          multiple input files cannot be specified!\n";

		std::cout << "\n      -out=path_to_output_file, -outfile=path_to_output_file";
		std::cout << "\n          specifies the output file to write to";
		std::cout << "\n          if -outmore=true or -outmore is provided, you can input multiple"
																			"output files\n";

		std::cout << "\n      -outmore=true, -outmore";
		std::cout << "\n          marks that multiple output files are provided\n";

		std::cout << "\n      -compare=true, -compare";
		std::cout << "\n          compares the output file(s) and input file.";
		std::cout << "\n          if any abilities are missing, it will report it\n";

		std::cout << "\n      -compareout=path_to_compare_output_file";
		std::cout << "\n          if this is provided, the output of comparision is wrote to file\n";

		std::cout << "\n      -color=RGBA_color";
		std::cout << "\n          specifies the color for hotkeys\n";

		std::cout << "\n      -hkcolor=RGBA_color";
		std::cout << "\n          specifies the color of hotkeys already in file";
		std::cout << "\n          You can also type -hkcolor=color, which specifies that";
		std::cout << "\n          both hkcolor and color are the same\n";

		std::cout << "\n      -levelColor=RGBA_color";
		std::cout << "\n          specifies the color of the levels in upgrades for instance";
		std::cout << "\n          if left empty, it is the same one as provided in -color";
		std::cout << "\n          if that color is also not set, the colors are not removed\n";

		std::cout << "\n      -remove=tip";
		std::cout << "\n          removes Tip from the output file\n";

		std::cout << "\n      -remove=untip";
		std::cout << "\n          removes Untip from the output file\n";

		std::cout << "\n      -remove=awakentip";
		std::cout << "\n          removes Awakentip from the output file\n";

		std::cout << "\n      -remove=revivetip";
		std::cout << "\n          removes Revivetip from the output file\n";

		std::cout << "\n      -remove=researchtip";
		std::cout << "\n          removes Researchtip from the output file\n";

		std::cout << "\n      -remove=hotkey";
		std::cout << "\n          removes Hotkey from the output file\n";

		std::cout << "\n      -remove=unhotkey";
		std::cout << "\n          removes Unhotkey from the output file\n";

		std::cout << "\n      -remove=researchhotkey";
		std::cout << "\n          removes Researchhotkey from the output file\n";

		std::cout << "\n      -remove=buttonpos";
		std::cout << "\n          removes Buttonpos from the output file\n";

		std::cout << "\n      -remove=unbuttonpos";
		std::cout << "\n          removes Unbuttonpos from the output file\n";

		std::cout << "\n      -remove=researchbuttonpos";
		std::cout << "\n          removes Researchbuttonpos from the output file\n";

		std::cout << "\n      -remove=tips";
		std::cout << "\n          removes all tips\n";

		std::cout << "\n      -remove=all";
		std::cout << "\n          removes all possible data(no rawcodes or names)\n";

		std::cout << "\n      -debug=true, -debug";
		std::cout << "\n          if true is passed or just debug is provided"
											 " , the program will output debug info\n";

		std::cout << "\n      -cpuclock=NUMBER(Ghz, Mhz, Khz)";
		std::cout << "\n          if printtime is used, this program will output";
		std::cout << "\n          how long it took for each phase to complete.";
		std::cout << "\n          if you dont provide clock, program will try to calcuate it for you";
		std::cout << "\n          Notice: You dont need to input Hz or hz if you write number alone\n";

		std::cout << "\n      -printtime=true, -printtime";
		std::cout << "\n          if this switch is specified, the application will print the time it";
		std::cout << "\n          for each operation to complete\n";

		std::cout << "\n      -help, --help, /help, help";
		std::cout << "\n          shows this message\n";

		std::cout << "\n      -color, -levelColor are not yet Implemented and do nothing!\n";

		std::cout << "\n      -keyboard=";
		std::cout << "\n          if this is specified, it will generate the file with";
		std::cout << "\n          given keyboard layout.";
		std::cout << "\n          Available layouts: qwery, qwerz, azerty";
		std::cout << "\n          Or: german, english, french";

		std::cout << "\n\n";

		return EXIT_SUCCESS;
	}

	RemoveMask mask = RemoveMask(0);
	
	std::vector<std::string> input;

	for(int i = argc - 1; i >= 0; --i)
	{
		input.push_back(std::string(argv[i]));
	}

	//additional scope to remove the vectors from memory
	//when they are no longer needed
	{
		std::vector<std::string> maskStrVec;
	
		maskStrVec.push_back("-remove=tip");
		maskStrVec.push_back("-remove=untip");
		maskStrVec.push_back("-remove=revivetip");
		maskStrVec.push_back("-remove=awakentip");
		maskStrVec.push_back("-remove=researchtip");
		maskStrVec.push_back("-remove=hotkey");
		maskStrVec.push_back("-remove=unhotkey");
		maskStrVec.push_back("-remove=researchhotkey");
		maskStrVec.push_back("-remove=buttonpos");
		maskStrVec.push_back("-remove=unbuttonpos");
		maskStrVec.push_back("-remove=researchbuttonpos");

		std::vector<RemoveMask> maskVec;
	
		maskVec.push_back(RemoveMask::Tip);
		maskVec.push_back(RemoveMask::Untip);
		maskVec.push_back(RemoveMask::ReviveTip);
		maskVec.push_back(RemoveMask::AwakenTip);
		maskVec.push_back(RemoveMask::ResearchTip);
		maskVec.push_back(RemoveMask::Hotkey);
		maskVec.push_back(RemoveMask::Unhotkey);
		maskVec.push_back(RemoveMask::Researchhotkey);
		maskVec.push_back(RemoveMask::Buttonpos);
		maskVec.push_back(RemoveMask::Unbuttonpos);
		maskVec.push_back(RemoveMask::Researchbuttonpos);

		for(int i = 0, j = maskStrVec.size(); i < j; ++i)
		{
			if (isSet(maskStrVec[i], input))
				mask = mask | maskVec[i];
		}
		if (isSet("-remove=tips", input))
		{
			mask = mask | RemoveMask::Tip | RemoveMask::Untip | RemoveMask::ReviveTip
						| RemoveMask::AwakenTip | RemoveMask::ResearchTip;
		}
		if (isSet("-remove=all", input))
		{
			for(auto&& a : maskVec)
				mask = mask | a;
		}
	}
	
	bool multipleOutput = isSet("-outmore=true", input) || isSet("-outmore", input);

	auto color = getColor(input);
	auto colorIn = getColorIn(input);
	auto levelColor = getColorLevel(input);

	if (colorIn == "color")
		colorIn = color;

	if (!levelColor.size())
		levelColor = color;

	auto inputFilePath = getInputFilePath(input);
	auto outputFilePath = getOutputFilePath(input);

	if (!inputFilePath.size())
	{
		std::cout << "No input file provided!\nTerminating!\n";

		std::exit(EXIT_FAILURE);
	}

	if (!outputFilePath.size())
	{
		std::cout << "No output file provided!\nTerminating!\n";
	}
	else if (outputFilePath.size() > 1 && !multipleOutput)
	{
		std::cout << "You have provided mutliple output files without ";
		std::cout << "using -outmore=true or -outmore.\n";

		std::cout << "Terminating!\n";

		std::exit(EXIT_FAILURE);
	}

	std::fstream inputFile(inputFilePath, std::fstream::in);

	if (!inputFile.is_open())
	{
		std::cout << "Couldnt open " << inputFilePath << "!\n";

		std::cout << "Terminating process!\n";
		return EXIT_FAILURE;
	}

	std::cout << "\n";

	ParseData dat;

	dat.inFile = std::move(inputFile);
	inputFile.swap(std::fstream());

	dat.debugMode = isSet("-debug=true", input) || isSet("-debug", input);
	dat.hkcolor = colorIn;
	dat.color = color;
	dat.multipleOutputs = isSet("-outmore=true", input) || isSet("-outmore", input);
	dat.outFiles = outputFilePath;
	dat.mask = mask;
	dat.cpuClock = getCpuClock(input);
	dat.showTimeOutput = isSet("-printtime=true", input) || isSet("-printtime", input);
	dat.levelColor = levelColor;
	dat.compare = isSet("-compare=true", input) || isSet("-compare", input);
	dat.inFilePath = inputFilePath;
	dat.compareOutput = getCompareOutputFilePath(input);
	dat.layout = getKeyboardLayout(input);

	parse(dat);
}