#ifndef _STRING_ITERATOR_H_
#define _STRING_ITERATOR_H_

#include "basicutils.h"
#include <vector>
#include <string>
#include <functional>
#include <algorithm>

namespace lg{
	class StringIterator{
		std::string container;
		std::string delim_list;
		uint currentPos;
		bool _lock;

		void _unlock_move_iterator()
		{
			_lock = false;
		}

		void _lock_move_iterator()
		{
			_lock = true;
		}

		//find if the char c is matching any chars from
		//delim_list(list of delimiters)
		bool _match_any_delimiter(const char c)
		{
			for(int i = delim_list.size()-1; i >= 0; --i)
			{
				if (delim_list[i] == c)	return true;
			}
			return false;
		}

		//starts at the currentPos, goes until the end of string
		//if not found, returns npos
		uint _find_closest_delim()
		{
			uint u = container.size();
			for(uint i = currentPos; i < u; ++i)
			{
				if (_match_any_delimiter(container[i]))
					return i;
			}
			return _end;
		}

		void _reset()
		{
			container = "";
			delim_list = " \n\t\r";
			currentPos = 0;
			_lock = false;
		}
	public:
		static const uint32 _end = -1;

		StringIterator() : container(), delim_list(),\
						currentPos(), _lock(false)
		{
		}

		//default constructor
		StringIterator(const std::string& intialWords, std::string delimiters = "\n\t\r ")
						: container(intialWords), delim_list(delimiters),
						currentPos(), _lock(false)
		{
		}

		//move construct
		StringIterator(StringIterator&& other) : container(std::move(other.container)),
						delim_list(std::move(other.delim_list)),
						currentPos(std::move(other.currentPos)),
						_lock(std::move(other._lock))
		{
			other._reset();
		}

		StringIterator(const StringIterator& l) : container(l.container), delim_list(l.delim_list),
													currentPos(l.currentPos), _lock(l._lock)
		{
		}

		StringIterator& operator=(const StringIterator& l)
		{
			container = l.container;
			delim_list = l.delim_list;
			currentPos = l.currentPos;
			_lock = l._lock;

			return *this;
		}

		StringIterator& operator=(StringIterator&& l)
		{
			std::swap(container, l.container);
			std::swap(delim_list, l.delim_list);
			std::swap(currentPos, l.currentPos);
			std::swap(_lock, l._lock);

			l._reset();

			return *this;
		}

		void move(int offset)
		{
			if (offset < 0)
			{
				if (currentPos - offset < 0)
					currentPos = 0;
				else
					currentPos += offset;
			}
			else
			{
				if (currentPos + offset > container.size())
					currentPos = container.size();
				else
					currentPos += offset;
			}
		}

		//move the position to new place
		void moveTo(uint newPos)
		{
			if (newPos < container.size()-1)
				if (newPos <= 0)	currentPos = 0;
				else				currentPos = newPos;
			else
				currentPos = container.size()-1;
		}

		//moves the iterator to the beginning of the last line
		void moveToLastLine()
		{
			bool currentLineEnc = false;

			for(int i = currentPos; i >= 0; --i)
			{
				if (container[i] == '\n')
					if (currentLineEnc)
					{
						currentPos = ++i;
						return;
					}
					else
						currentLineEnc = true;
			}
		}

		void setString(const std::string& newStr)
		{
			moveTo(0);
			container = newStr;
		}

		std::string getString() const
		{
			return container;
		}

		//change the list of letters that are considered delimiters
		void setDelimiterList(const std::string& newDelimList)
		{
			delim_list = newDelimList;
		}
		
		const std::string& getDelimiterList() const { return delim_list; }

		//get the size of the container string
		uint getSize()
		{
			return container.size();
		}

		//returns the distance from current position to next delimiter
		uint getNextWordLength()
		{
			return peek(std::string::npos).size();
		}

		//returns length of the next word even if you are in the
		//middle of it
		uint getNextFullWordLength()
		{
			return peekSpecial(std::string::npos).size();
		}

		//reads all the content of the string inside iterator
		//does not move the position and starts at 0
		//not at current position of pointer
		std::vector<char> readBinaryAll()
		{
			if (atEnd())	return std::vector<char>();

			bool locked = _lock;
			_unlock_move_iterator();

			std::vector<char> ret;

			for(uint i = 0, j = container.size(); i < j; ++i)
			{
				ret.push_back(container.at(i));
			}
			return ret;
		}

		//reads all the content of the string inside iterator
		//does not move the position and starts at 0
		//not at current position of pointer
		std::string readAll()
		{
			if (atEnd())	return "";

			bool locked = _lock;
			_unlock_move_iterator();

			std::string ret;

			for(uint i = 0, j = container.size(); i < j; ++i)
			{
				ret.push_back(container.at(i));
			}
			return ret;
		}

		std::vector<char> readBinary(uint howMany = _end, bool stopOnDelimiter = true)
		{
			if (atEnd())	return std::vector<char>();

			if (start())
				while(_match_any_delimiter(container.at(currentPos)) ||
						!container.at(currentPos))
						++currentPos;

			bool locked = _lock;
			_unlock_move_iterator();

			auto nextDelim = _find_closest_delim();

			std::vector<char> ret;

			//if requested more and there is delimiter
			//and stop on it
			if (nextDelim != _end && stopOnDelimiter)
			{
				ret.reserve(nextDelim - currentPos);

				for(uint i = currentPos, j = nextDelim; i < j; ++i)
				{
					ret.push_back(container.at(i));
				}
				if (!locked)
					currentPos = ++nextDelim;
			}
			else if (howMany >= (container.size()-1-currentPos))
			{
				//if requested more and there isnt delimiter
				//or dont stop on it
				ret.reserve(container.size() - currentPos);

				for(int i = currentPos, j = container.size(); i < j; ++i)
				{
					ret.push_back(container.at(i));
				}
				if (!locked)
					currentPos = container.size();
			}
			//if less required
			else
			{
				ret.reserve(howMany);

				//if there isnot delim or we dont stop on it
				for(uint i = currentPos, j = currentPos + howMany; i < j; ++i)
				{
					ret.push_back(container.at(i));
				}
				if (!locked)
					currentPos += ++howMany;
			}
			return ret;
		}

		//reads until the delim_list was found in one piece(so if the delim_list is
		//ABC, than this reads either until end is found or until ABC is found)
		//if the end is found, returns empty string
		std::string readExact()
		{
			if (atEnd())	return "";

			bool locked = _lock;
			_unlock_move_iterator();

			std::string str;

			bool found = false;
			int cIter = currentPos;
			std::string checker = delim_list;
			std::string inter = container.substr(cIter, checker.size());

			bool firstIter = true;

			while(!found)
			{
				if (cIter >= container.size()-1)		return "";

				if (inter == checker)
				{
					found = true;
				}
				else
				{
					inter = container.substr(++cIter, checker.size());
				}
				firstIter = false;
			}

			if (firstIter)
				throw 1;

			for(int i = currentPos, j = cIter; i < j; ++i)
			{
				str.push_back(container.at(i));
			}

			if (!locked)
				currentPos = cIter;

			return str;
		}

		//reads "howMany" characters from the string
		//if stopOnDelimiter is not set or set to true, the reading
		//stops at the next delimiter
		//if false is passed as second argument, it reads exactly howMany letters
		//call to this function does move the iterator forward
		std::string read(uint howMany = _end, bool stopOnDelimiter = true)
		{
			if (atEnd())	return "";

			while(currentPos < container.size() && (_match_any_delimiter(container.at(currentPos)) ||
					!container.at(currentPos)))
					++currentPos;

			bool locked = _lock;
			_unlock_move_iterator();

			auto nextDelim = _find_closest_delim();

			std::string ret;

			//if requested more and there is delimiter
			//and stop on it
			if (nextDelim != _end && stopOnDelimiter)
			{
				ret.reserve(nextDelim - currentPos);

				for(uint i = currentPos, j = nextDelim; i < j; ++i)
				{
					ret.push_back(container.at(i));
				}
				if (!locked)
					currentPos = ++nextDelim;
			}
			else if (howMany >= (container.size()-1-currentPos))
			{
				//if requested more and there isnt delimiter
				//or dont stop on it
				ret.reserve(container.size() - currentPos);

				for(int i = currentPos, j = container.size(); i < j; ++i)
				{
					ret.push_back(container.at(i));
				}
				if (!locked)
					currentPos = container.size();
			}
			//if less required
			else
			{
				ret.reserve(howMany);

				//if there isnot delim or we dont stop on it
				for(uint i = currentPos, j = currentPos + howMany; i < j; ++i)
				{
					ret.push_back(container.at(i));
				}
				if (!locked)
					currentPos += ++howMany;
			}
			return ret;
		}

		void operator>>(float& val)
		{
            if (atEnd())
            {
                val = 0;
                return;
            }

            val = std::stof(read());
		}

		void operator>>(int& val)
		{
            if (atEnd())
            {
                val = 0;
                return;
            }


            val = std::stoi(read());
		}

		void operator>>(std::string& val)
		{
            val = (read());
		}

		template <class U>
		void operator>>(U& val)
		{
            if (atEnd())
            {
                auto a = U();
                std::swap(val, a);
                return;
            }

            val = (U)read();
		}

		void movePastNextDelimiter()
		{
			while(_match_any_delimiter(currentPos))	++currentPos;
		}

		//reads "howMany" characters from the string from the beginning of the
		//last word(if stopped at the middle of word, it moves backwards until it
		//finds another delimiter)
		//if stopOnDelimiter is not set or set to true, the reading
		//stops at the next delimiter
		//if false is passed as second argument, it reads exactly howMany letters
		//call to this function does move the iterator forward
		std::string readSpecial(uint howMany, bool stopOnDelimiter = true)
		{
			if (currentPos == 0)	read(howMany, stopOnDelimiter);
			while(!_match_any_delimiter(container[currentPos])) { --currentPos; }
			return read(howMany, stopOnDelimiter);
		}

		//reads "howMany" characters from the string
		//if stopOnDelimiter is not set or set to true, the reading
		//stops at the next delimiter
		//if false is passed as second argument, it reads exactly howMany letters
		//call to this function does not move the iterator forward
		std::string peek(uint howMany = _end, bool stopOnDelimiter = true)
		{
			//lock the moving
			_lock_move_iterator();
			//read howMany characters
			return read(howMany, stopOnDelimiter);
		}

		//reads "howMany" characters from the string from the beginning of the
		//last word(if stopped at the middle of word, it moves backwards until it
		//finds another dimiter)
		//if stopOnDelimiter is not set or set to true, the reading
		//stops at the next delimiter
		//if false is passed as second argument, it reads exactly howMany letters
		//call to this function does not move the iterator forward
		std::string peekSpecial(uint howMany = _end, bool stopOnDelimiter = true)
		{
			if (currentPos == 0)	peek(howMany, stopOnDelimiter);
			while(!_match_any_delimiter(container[currentPos])) { --currentPos; }
			return peek(std::string::npos, stopOnDelimiter);
		}

		//returns the next delimiter that will be found on the string in pair of
		//<the character, position>
		std::pair<uint8, uint> getNextDelimiter()
		{
			auto cPos = currentPos;
			for(uint u = cPos; u < container.size(); ++u)
				if (_match_any_delimiter(container[u]))
					return std::make_pair(container.at(u), u);

			return std::make_pair(0, _end);
		}

		//returns the last(next if calling reverse member functions) delimiter
		//that was(will be) found in pair of <the character, position>
		std::pair<uint8 , uint> getLastDelimiter()
		{
			auto cPos = currentPos;
			for(uint u = cPos; u < container.size(); --u)
				if (_match_any_delimiter(container[u]))
					return std::make_pair(container.at(u), u);

			return std::make_pair(0, _end);
		}

		inline bool atEnd()
		{
			if (!container.size())	return true;
			else return currentPos >= container.size();
		}

		//return if the iterator is at the start
		inline bool start() { return currentPos <= 0; }

		//return current position of iterator as integral value
		inline uint getPosition() { return currentPos; }

		//Warning! indexes begin at 0 !
		inline uint currentLineNumber()
		{
			int lineCount = 0;
			for(int i = 0; i <= currentPos && i < container.size(); ++i)
			{
				if (container[i] == '\n')
					lineCount++;
			}

			return lineCount;
		}
	};
};

#endif	//_STRING_ITERATOR_H_
