
#ifndef _PARSER_H_
#define _PARSER_H_


#include <vector>
#include <string>
#include <fstream>

#include <iostream>

#include "stringiterator.h"

extern std::ostream std::cout;

enum RemoveMask{
	Tip =				1 << 0,
	Untip =				1 << 1,
	AwakenTip =			1 << 2,
	ReviveTip =			1 << 3,
	ResearchTip =		1 << 4,
	Hotkey =			1 << 5,
	Unhotkey =			1 << 6,
	Researchhotkey =	1 << 7,
	Buttonpos =			1 << 8,
	Unbuttonpos =		1 << 9,
	Researchbuttonpos = 1 << 10
};

struct KeyboardLayout{
	//pair is:
	//first - qwerty equivalent(my own, default one)
	//second - mapped to keyboard
	std::vector<std::pair<char, char>> mapped;

	//the "which" is checked in "first"
	//and if there is, returns "second"
	char map(char which) const
	{
		for(auto&& a : mapped)
		{
			if (a.first == which)
				return a.second;
		}

		return which; //the key itself
	}

	//is protected from double add
	//first argument is the original char, from qwertz keyboard
	//the second argument is the new mapped value
	void addMapping(char in, char newC)
	{
		if (!_contains(in))
		{
			mapped.push_back(std::make_pair(in, newC));
		}
	}
private:
	bool _contains(char c)
	{
		for(auto&& a : mapped)
		{
			if (a.first == c)
				return true;
		}
		return false;
	}
};

struct ParseData{
	std::fstream inFile;
	std::string inFilePath;
	std::vector<std::string> outFiles;
	std::string color;
	std::string hkcolor;
	std::string levelColor;
	RemoveMask mask;
	bool debugMode;
	bool multipleOutputs;
	long long cpuClock;
	bool showTimeOutput;
	bool compare;
	std::string compareOutput;
	KeyboardLayout layout;

	ParseData() : inFile(), inFilePath(), outFiles(), color(), hkcolor(), levelColor(), mask(),
					debugMode(0), multipleOutputs(0), cpuClock(0), showTimeOutput(0), compare(0),
					compareOutput(), layout()
	{
	}
};

RemoveMask operator&(RemoveMask mask, int value)
{
	return castToEnum<RemoveMask>(castEnum(mask) & value);
}

RemoveMask operator|(RemoveMask mask, int value)
{
	return castToEnum<RemoveMask>(castEnum(mask) | value);
}

RemoveMask operator^(RemoveMask mask, int value)
{
	return castToEnum<RemoveMask>(castEnum(mask) ^ value);
}


struct Ability{
	std::string name;
	std::string rawcode;
	std::string hotkey;
	
	std::string tip;
	std::string untip;
	std::string revivetip;
	std::string awakentip;

	std::string researchhotkey;
	std::string researchtip;

	std::string unhotkey;

	std::string buttonpos;
	std::string unbuttonpos;
	std::string researchbuttonpos;

	Ability() : name(), rawcode(), hotkey(), tip(), untip(), revivetip(), awakentip(),
				researchhotkey(), researchtip(), unhotkey(), buttonpos(), unbuttonpos(),
				researchbuttonpos()
	{
	}
};

#include <ctime>

namespace inner{
	inline long long cpuclock()
	{
		//get current clock
		int cT = clock();

		//while it is the same interval, wait
		while(clock() == cT)	;

		//give it another interval of wait for accuracy
		cT = clock();
		while(clock() == cT)	;

		//get time stamp clock on CPU
		long long cRd = __rdtsc();

		//reinit clock
		cT = clock();

		//wait the shortest amount possible with clock
		while(cT == clock())	;

		//get the time stamp clock after clock() - cT amount of time
		cRd = __rdtsc() - cRd;

		//multiply the time stamp clock by clocks_per_sec
		//and divide it by current clock() - the last clock, which
		//is stored inside cT
		//this will yield cpu clocks per second
		return cRd * CLOCKS_PER_SEC / (clock() - cT);
	}

	inline bool isChar(char c)
	{
		return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
	}

	inline bool isUpper(char c)
	{
		return c >= 'A' && c <= 'Z';
	}

	inline bool isLower(char c)
	{
		return c >= 'a' && c <= 'z';
	}
	
	inline char toUpper(char c)
	{
		const int offset = 'a' - 'A';

		if (isUpper(c))
			return c;
		
		return isChar(c) ? c - offset : c;
	}

	inline char toLower(char c)
	{
		const int offset = 'a' - 'A';

		if (isLower(c))
			return c;

		return isChar(c) ? c + offset : c;
	}

	inline bool isWhiteSpace(char c)
	{
		return c == ' ' || c == '\n' || c == '\t' || c == '\r';
	}

	inline void removeTrailingWhitespaces(std::string& str)
	{
		while(isWhiteSpace(str.back()))
			str.pop_back();
	}

	inline void removeStartingWhitespaces(std::string& str)
	{
		int a = 0;
		while(str.size() && isWhiteSpace(str[a]))
			++a;

		if (a)
			str = str.substr(a);
	}

	inline std::string lowerCase(const std::string& str)
	{
		std::string result;

		const int offset = 'a' - 'A';

		for(int i = 0, j = str.size(); i < j; ++i)
		{
			if (isUpper(str[i]))
				result.push_back(str[i] + offset);
			else
				result.push_back(str[i]);
		}

		return result;
	}

	std::string upperCase(const std::string& str)
	{
		std::string result;

		const int offset = 'a' - 'A';

		for(int i = 0, j = str.size(); i < j; ++i)
		{
			if (isLower(str[i]))
				result.push_back(str[i] - offset);
			else
				result.push_back(str[i]);
		}

		return result;
	}

	inline bool multipleHotkey(const std::string& str)
	{
		lg::StringIterator iter(str, ", \n\r\t");

		bool is = true;

		while(!iter.atEnd())
		{
			std::string read = iter.read();
			if (!(read.size() && (isUpper(read[0]) || read.substr(0, 3) == "512")))
			{
				is = false;
				break;
			}
		}

		return is;
	}

	inline bool isRawcode(const std::string& str)
	{
		lg::StringIterator iter(str, " \t\n");
		auto read = iter.read();

		//if it is less then 2 characters long, it cant be rawcode
		if (read.size() < 2)
			return false;

		//if first character is [ and last is ] then it is rawcode
		return read[0] == '[' && read.back() == ']';
	}

	inline bool isPureRawcode(const std::string& str)
	{
		if (str.size() < 4)
			return false;

		//if it is longer than 4 characters, it must be Cmd or it is not rawcode
		if (str.size() > 4)
			return upperCase(str.substr(0, 3)) == upperCase("Cmd");

		for(int i = 0, j = str.size(); i < j; ++i)
		{
			//if it is not 0 - 9, a - z or A - Z, it is not valid rawcode for melee maps
			char c = str[i];
			if (!((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')))
				return false;
		}

		return true;
	}
	
	inline std::string parseRawcode(const std::string& str)
	{
		if (!isRawcode(str))
			return "";

		int firstPos = 0;
		
		int size = str.size() - 1;

		//while there are whitespaces or it is [ letter, skip it
		while(firstPos <= size && isWhiteSpace(str[firstPos]) || str[firstPos] == '[')	firstPos++;

		int backOffset = 0;

		//while there are whitespaces or the current character is ] at the back, skip it
		while(backOffset <= size && isWhiteSpace(str[size-backOffset]) || str[size-backOffset] == ']')
			backOffset++;

		//calculate the last character as the size - the number skipped + 1
		//+1 because size is initially -1
		int lastPos = size - backOffset + 1;

		//return substring of string str in range [First Valid; Last Valid - First Valid]
		return str.substr(firstPos, lastPos - firstPos);
	}

	//this expects rawcode to be passed in without []!
	inline bool isAbility(const std::string& str)
	{
		if (!isPureRawcode(str))
			return false;

		//certain abilities start with S, all others start with A
		return str[0] == 'A' || str[0] == 'S';
	}
	
	//this expects rawcode to be passed in without []!
	inline bool isHeroAbility(const std::string& str)
	{
		if (!isPureRawcode(str))
			return false;

		return isUpper(str[1]);
	}
	
	//this expects rawcode to be passed in without []!
	inline bool isResearch(const std::string& str)
	{
		if (!isPureRawcode(str))
			return false;

		return str[0] == 'R';
	}

	inline bool isComment(const std::string& str)
	{
		lg::StringIterator iter(str, " \t");

		auto read = iter.read();
		
		if (read.size() < 2)
			return false;

		return read[0] == '/' && read[1] == '/';
	}
	
	inline std::string parseComment(const std::string& str)
	{
		if (str.size() < 2 || !isComment(str))
			return "";

		return str.substr(2);
	}

	inline std::string formatComment(const std::string& str)
	{
		lg::StringIterator iter(str, "//\n\r\t");
		std::string res;

		while(!iter.atEnd())
		{
			std::string buffer = iter.read();
			if (buffer.size())
				buffer[0] = toupper(buffer[0]);

			res += buffer + " ";
		}

		removeStartingWhitespaces(res);
		removeTrailingWhitespaces(res);

		return res;
	}

	inline bool isCommentBlock(const std::string& str)
	{
		lg::StringIterator iter(str, " \t");

		auto read = iter.read();

		if (read.size() < 3)
			return false;

		return read[0] == '/' && read[1] == '/' && read[2] == '/';
	}

	inline bool isHexadecimal(char c)
	{
		return ((c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F')) || (c >= '0' && c <= '9');
	}

	inline bool isHexa(char c)
	{
		return isHexadecimal(c);
	}

	inline bool isColor(const std::string& str)
	{
		if (str.size() < 10)
			return false;

		bool cStart = upperCase(str.substr(0, 4)) == upperCase("|cff");

		if (!cStart)
			return false;

		for(int i = 4; i < 10; ++i)
		{
			if (!isHexa(str[i]))
				return false;
		}
		return true;
	}

	inline bool isColorType(const std::string& str, const std::string& color)
	{
		if (!isColor(str))
			return false;

		return str.substr(2) == color;
	}

	inline std::string readFile(std::fstream& file)
	{
		//get the size of the file
		file.seekg(0, std::fstream::end);
		int fileSize = file.tellg();


		//reset position of the file
		file.seekg(0);
		file.seekp(0);

		std::string fileContent;
	
		//grow the string to be sufficiently large for the whole file
		fileContent.resize(fileSize, ' ');
	
		//read everything from file
		file.read(&fileContent[0], fileSize);

		return fileContent;
	}

	inline void _loadIterator(lg::StringIterator& fromIterator, std::string& toString,
								const std::string& tempDelimList)
	{
		//we first move the iterator 1 character back
		//so that it always reads something
		fromIterator.move(-1);

		//store the list, and replace it with new one
		auto list = fromIterator.getDelimiterList();
		fromIterator.setDelimiterList(tempDelimList);

		//read from iterator
		toString = fromIterator.read();

		//we must erase the first character read, because it is garbage
		//most likely =
		toString.erase(0, 1);

		//restore the delimiter list
		fromIterator.setDelimiterList(list);
	}

	//we must check for both \n and \n\r, because windows
	inline bool isWindowsNewLine(const std::string& str)
	{
		return (str.back() == '\r' && str[str.size()-2] == '\n')
				|| str.back() == '\n';
	}

	inline std::vector<Ability> load(const std::string& string, bool debug)
	{
		lg::StringIterator iterator(string, "\n\r=");

		std::vector<Ability> list;
		
		while(!iterator.atEnd())
		{
			std::string read = iterator.read();

			//if what we've got is comment
			//but it isnt comment block
			if (isComment(read) && !isCommentBlock(read))
			{
				std::string next = iterator.peek();

				//and the next thing after comment is rawcode
				if (isRawcode(next))
				{
					Ability abil;

					//store the comment as name
					abil.name = read;

					//we know for sure that this is rawcode
					read = iterator.read();

					abil.rawcode = parseRawcode(read);

					//read from iterator
					read = iterator.read();

					//then until we find another comment or rawcode we read from iterator
					//assuming that there are no comments between data
					while(!isComment(read) && !isRawcode(read) && !iterator.atEnd())
					{
						//lets upper case what we've read
						read = upperCase(read);

						//if it is tip
						if (read == upperCase("Tip"))
							_loadIterator(iterator, abil.tip, "\n\r");
					
						//else if it is hotkey
						//hotkey requires special treatment
						else if (read == upperCase("Hotkey"))
						{
							_loadIterator(iterator, read, "\n\r");
							abil.hotkey = read;
						}

						//else if it is untip
						else if (read == upperCase("Untip"))
							_loadIterator(iterator, abil.untip, "\n\r");
					
						//and so on
						else if (read == upperCase("Unhotkey"))
							_loadIterator(iterator, abil.unhotkey, "\n\r");
					
						else if (read == upperCase("Revivetip"))
							_loadIterator(iterator, abil.revivetip, "\n\r");
					
						else if (read == upperCase("Awakentip"))
							_loadIterator(iterator, abil.awakentip, "\n\r");
					
						else if (read == upperCase("Researchtip"))
							_loadIterator(iterator, abil.researchtip, "\n\r");
					
						else if (read == upperCase("Researchhotkey"))
							_loadIterator(iterator, abil.researchhotkey, "\n\r");
					
						else if (read == upperCase("Buttonpos"))
							_loadIterator(iterator, abil.buttonpos, "\n\r");
					
						else if (read == upperCase("Unbuttonpos"))
							_loadIterator(iterator, abil.unbuttonpos, "\n\r");
					
						else if (read == upperCase("Researchbuttonpos"))
							_loadIterator(iterator, abil.researchbuttonpos, "\n\r");

						read = iterator.read();
					}
					list.push_back(abil);

					if (!iterator.atEnd())
						//at this point, we've read the comment or rawcode of next ability out
						//so we move iterator one line back
						iterator.moveToLastLine();
				}
			}
			//at this point, the ability doesnt have comment but is ability
			else if (isRawcode(read))
			{
				Ability abil;

				abil.name = "";

				abil.rawcode = parseRawcode(read);

				read = iterator.read();

				//then until we find another comment or rawcode we read from iterator
				//assuming that there are no comments between data
				while(!isComment(read) && !isRawcode(read) && !iterator.atEnd())
				{
					//lets upper case what we've read
					read = upperCase(read);

					//if it is tip
					if (read == upperCase("Tip"))
						_loadIterator(iterator, abil.tip, "\n\r");
					
					//else if it is hotkey
					//hotkey requires special treatment
					else if (read == upperCase("Hotkey"))
					{
						_loadIterator(iterator, read, "\n\r");
						abil.hotkey = read;
					}

					//else if it is untip
					else if (read == upperCase("Untip"))
						_loadIterator(iterator, abil.untip, "\n\r");
					
					//and so on
					else if (read == upperCase("Unhotkey"))
						_loadIterator(iterator, abil.unhotkey, "\n\r");
					
					else if (read == upperCase("Revivetip"))
						_loadIterator(iterator, abil.revivetip, "\n\r");
					
					else if (read == upperCase("Awakentip"))
						_loadIterator(iterator, abil.awakentip, "\n\r");
					
					else if (read == upperCase("Researchtip"))
						_loadIterator(iterator, abil.researchtip, "\n\r");
					
					else if (read == upperCase("Researchhotkey"))
						_loadIterator(iterator, abil.researchhotkey, "\n\r");
					
					else if (read == upperCase("Buttonpos"))
						_loadIterator(iterator, abil.buttonpos, "\n\r");
					
					else if (read == upperCase("Unbuttonpos"))
						_loadIterator(iterator, abil.unbuttonpos, "\n\r");
					
					else if (read == upperCase("Researchbuttonpos"))
						_loadIterator(iterator, abil.researchbuttonpos, "\n\r");

					read = iterator.read();
				}
				list.push_back(abil);

				if (!iterator.atEnd())
					//at this point, we've read the comment or rawcode of next ability out
					//so we move iterator one line back
					iterator.moveToLastLine();
			}
		}
		return list;
	}

	inline std::vector<Ability> load(std::fstream& file, bool debug)
	{
		return load(readFile(file), debug);
	}
	/*
	//load every "Ability" inside the file
	inline std::vector<Ability> load(std::fstream& fromFile, bool isDebug)
	{
		using namespace lg;
	
		//read everything from the file
		StringIterator iterator(readFile(fromFile), "\n\r=");

		//we no longer need the file to be opened
		fromFile.close();

		//construct vector
		std::vector<Ability> list;

		//while there is stuff to read
		while(!iterator.atEnd())
		{
			//instanciate new ability
			Ability abil;

			//read from iterator
			std::string read = iterator.read();

			if (isDebug)
				std::cout << "Reading " << read << " from file\n";

			//if we have read comment block
			if (isCommentBlock(read))
			{
				//then while we read comments or comment blocks
				while(isCommentBlock(read) || isComment(read))
				{
					//store them
					abil.name += read + "\n";

					//assign new to move with iterator
					read = iterator.read();

					//if the next thing is comment block and we have comment block
					if (isCommentBlock(iterator.peek()) && isCommentBlock(read))
						//insert 2 lines
						abil.name += read + "\n\n";
				}

				//at this point we have read something else
				//most likely rawcode
				//so we move iterator back twice to get the comment too
				iterator.moveToLastLine();
				iterator.moveToLastLine();

				//and we remove the last commented line from the string
				//we must however check if we didnt extract everything

				while(abil.name.size() && isWindowsNewLine(abil.name))
					abil.name.pop_back();

				while(abil.name.size() && (!isWindowsNewLine(abil.name)))
				{
					//we pop it, but this way we keep the last new line
					abil.name.pop_back();
				}

				//then store it
				list.push_back(abil);
			}
			else if (isComment(read))
			{
				//if it is comment, and it is not within comment block
				//it is most likely ability's name, and lets assume it is
				abil.name = read;

				//lets assume there is no more than 1 comment per ability
				//so while we didnt hit next comment, it is still this ability
				//we must also check if the iterator is not at the end
				while(!isComment(read = iterator.read()) && !iterator.atEnd())
				{
					if (isRawcode(read))
					{
						abil.rawcode = parseRawcode(read);
					}

					//lets upper case what we've read
					read = upperCase(read);

					//if it is tip
					if (read == upperCase("Tip"))
						_loadIterator(iterator, abil.tip, "\n\r");
					
					//else if it is hotkey
					//hotkey requires special treatment
					else if (read == upperCase("Hotkey"))
					{
						_loadIterator(iterator, read, "\n\r");
						abil.hotkey = read;
					}

					//else if it is untip
					else if (read == upperCase("Untip"))
						_loadIterator(iterator, abil.untip, "\n\r");
					
					//and so on
					else if (read == upperCase("Unhotkey"))
						_loadIterator(iterator, abil.unhotkey, "\n\r");
					
					else if (read == upperCase("Revivetip"))
						_loadIterator(iterator, abil.revivetip, "\n\r");
					
					else if (read == upperCase("Awakentip"))
						_loadIterator(iterator, abil.awakentip, "\n\r");
					
					else if (read == upperCase("Researchtip"))
						_loadIterator(iterator, abil.researchtip, "\n\r");
					
					else if (read == upperCase("Researchhotkey"))
						_loadIterator(iterator, abil.researchhotkey, "\n\r");
					
					else if (read == upperCase("Buttonpos"))
						_loadIterator(iterator, abil.buttonpos, "\n\r");
					
					else if (read == upperCase("Unbuttonpos"))
						_loadIterator(iterator, abil.unbuttonpos, "\n\r");
					
					else if (read == upperCase("Researchbuttonpos"))
						_loadIterator(iterator, abil.researchbuttonpos, "\n\r");
				}

				//at this point, we have already read the comment of next ability
				//so lets push it back
				iterator.moveToLastLine();

				//everything possible about the line has been read, push it to the list
				list.push_back(abil);
			}
			else
			{
				//at this point, we are not in comment block, neither the ability has comment
				abil.name = "";

				if (isRawcode(read))
				{
					//if it is rawcode
					abil.rawcode = parseRawcode(read);
					
					//while we didnt find another rawcode
					while(!isRawcode(read = iterator.read()) && !iterator.atEnd())
					{
						//if we found comment
						//lets assume there are no comments in between
						if (isComment(read))
						{
							//therefore, we are in another ability already
							//then we need to move the iterator back before the comment
							iterator.moveToLastLine();
							break;
						}
					}
				}
				else
				{
					//at this point, there is something that is not attached to anything

					std::cout << read << " entry was found without being bound to ability or";
					std::cout << " being comment, or within comment block! at line "
							  << iterator.currentLineNumber() + 1 << "\n";
				}
			}
		}

		return list;
	}
	*/

	inline std::string purgeWhiteSpaces(const std::string& str)
	{
		std::string s;
		for(int i = 0, j = str.size(); i < j; ++i)
		{
			if (!isWhiteSpace(str[i]))
				s.push_back(str[i]);
		}
		return s;
	}

	inline std::string createString(const std::vector<Ability>& vec, RemoveMask mask,
										const KeyboardLayout& layout)
	{
		std::string output;

		for(auto&& a : vec)
		{
			if (a.name.size())
				output += a.name + "\n";

			if (a.rawcode.size())
				output += "[" + a.rawcode + "]\n";

			if (a.hotkey.size() && !(mask & RemoveMask::Hotkey))
			{
				std::string s = purgeWhiteSpaces(a.hotkey);

				//it is multihotkey
				if (s[1] == ',')
					output += "Hotkey=" + s + "\n";
				else
				{
					//it is either 512 or invalid
					if (s.size() > 1)
					{
						//if it is 512, insert it
						if (s == "512")
							output += "Hotkey=512\n";
					}
					else
					{
						std::string out = "Hotkey=";
						out.push_back(layout.map(s[0]));
						out.push_back('\n');

						output += out;
					}
				}
			}

			if (a.unhotkey.size() && !(mask & RemoveMask::Unhotkey))
			{
				//special treatment because of way visual allocates space
				//for strings
				if (a.unhotkey.size() > 3 && !multipleHotkey(a.unhotkey))
				{
					std::string toPrint = a.unhotkey;
					removeTrailingWhitespaces(toPrint);
					
					toPrint[0] = layout.map(toPrint[0]);

					output += "Unotkey=" + toPrint + "\n";
				}
				else if (a.unhotkey.size() > 3 && multipleHotkey(a.unhotkey))
				{
					output += "Unotkey=" + a.unhotkey + "\n";
				}
				else
				{
					if (a.unhotkey == "512")
						output += "Unotkey=512\n";
					else
					{
						std::string s = "Unotkey=";
						s.push_back(layout.map(a.unhotkey[0]));
						s += "\n";
						output += s;
					}
				}
			}
		
			if (a.researchhotkey.size() && !(mask & RemoveMask::Researchhotkey))
			{
				//special treatment because of way visual allocates space
				//for strings
				if (a.researchhotkey.size() > 3 && !multipleHotkey(a.researchhotkey))
				{
					std::string toPrint = a.researchhotkey;
					removeTrailingWhitespaces(toPrint);

					toPrint[0] = layout.map(toPrint[0]);

					output += "Researchhotkey=" + toPrint + "\n";
				}
				else if (a.researchhotkey.size() > 3 && multipleHotkey(a.researchhotkey))
				{
					output += "Researchhotkey=" + a.researchhotkey + "\n";
				}
				else
				{
					if (a.researchhotkey == "512")
						output += "Researchhotkey=512\n";
					else
					{
						std::string s = "Researchhotkey=";
						s.push_back(layout.map(a.researchhotkey[0]));
						s += "\n";
						output += s;
					}
				}
			}

			if (a.tip.size() && !(mask & RemoveMask::Tip))
				output += "Tip=" + a.tip + "\n";

			if (a.untip.size() && !(mask & RemoveMask::Untip))
				output += "Untip=" + a.untip + "\n";

			if (a.researchtip.size() && !(mask & RemoveMask::ResearchTip))
				output += "Researchtip=" + a.researchtip + "\n";
		
			if (a.revivetip.size() && !(mask & RemoveMask::ReviveTip))
				output += "Revivetip=" + a.revivetip + "\n";

			if (a.awakentip.size() && !(mask & RemoveMask::AwakenTip))
				output += "awakentip=" + a.awakentip + "\n";
		
			if (a.buttonpos.size() && !(mask & RemoveMask::Buttonpos))
				output += "Buttonpos=" + a.buttonpos + "\n";
		
			if (a.unbuttonpos.size() && !(mask & RemoveMask::Unbuttonpos))
				output += "Unbuttonpos=" + a.unbuttonpos + "\n";
		
			if (a.researchbuttonpos.size() && !(mask & RemoveMask::Researchbuttonpos))
				output += "Researchbuttonpos=" + a.researchbuttonpos + "\n";

			output += "\n";
		}

		return output;
	}

	inline void writeResult(ParseData& data, std::string& outputString)
	{
		auto& filePath = data.outFiles;
		
		if (outputString.size())
			inner::removeTrailingWhitespaces(outputString);

		if (outputString.size())
			inner::removeStartingWhitespaces(outputString);

		for(int i = 0, j = filePath.size(); i < j; ++i)
		{
			std::fstream file(filePath[i], std::fstream::out | std::fstream::trunc);

			if (!file.is_open())
			{
				std::cout << "Couldn't open " << filePath[i] << "!\n";

				if (data.multipleOutputs)
				{
					std::cout << "Skipping\n";
					file.close();
					continue;
				}
				else
				{
					std::cout << "Terminating!\n";
					file.close();

					std::exit(EXIT_FAILURE);
				}
			}

			file.write(outputString.c_str(), outputString.size());

			file.flush();

			file.close();
		}
	}

	inline bool containsColorCodes(const std::string& str)
	{
		for(int i = 0, j = str.size() - 10; i < j; ++i)
		{
			if (isColor(str.substr(i, 10)))
				return true;
		}
		return false;
	}

	inline bool containsColorCodesType(const std::string& str, const std::string& color)
	{
		for(int i = 0, j = str.size() - 10; i < j; ++i)
		{
			if (isColorType(str.substr(i, 10), color))
				return true;
		}
		return false;
	}
	/*
	inline std::string checkRemoveColorCode(const std::string& input, const std::string& color)
	{
		std::string res = input;

		int colorCounter = 0;
		std::vector<std::pair<int, int>> toRemove;

		for(int i = 0, j = input.size(); i < j; ++i)
		{
			std::string sub = input.substr(i, 10);

			if (isColor(sub))
			{
				if (isColorType(sub, color))
				{
					colorCounter++;
					toRemove.emplace_back(i, 10);
				}
			}
			else if	(input.substr(i, 2) == "|r")
			{
				colorCounter--;

				if (!colorCounter)
				{
					toRemove.emplace_back(i, 2);
					break;
				}
			}
		}

		std::sort(toRemove.begin(), toRemove.end(), [](const std::pair<int, int>& l,
														const std::pair<int, int>& r){
			return l.first > r.first;
		});

		for(int i = 0, j = toRemove.size(); i < j; ++i)
		{
			res.erase(toRemove[i].first, toRemove[i].second);
		}

		return containsColorCodesType(res, color) ? checkRemoveColorCode(res, color) : res;
	}
	*/

	inline void checkRemoveColorCode(std::string& str, const std::string& color)
	{
		int colorCounter = 0;
		std::vector<std::pair<int, int>> toRemove;

		for(int i = 0, j = str.size(); i < j; ++i)
		{
			std::string sub = str.substr(i, 10);

			if (isColor(sub))
			{
				if (isColorType(sub, color))
				{
					colorCounter++;
					toRemove.emplace_back(i, 10);
				}
			}
			else if	(str.substr(i, 2) == "|r")
			{
				colorCounter--;

				if (!colorCounter)
				{
					toRemove.emplace_back(i, 2);
					break;
				}
			}
		}

		std::sort(toRemove.begin(), toRemove.end(), [](const std::pair<int, int>& l,
														const std::pair<int, int>& r){
			return l.first > r.first;
		});

		for(int i = 0, j = toRemove.size(); i < j; ++i)
		{
			str.erase(toRemove[i].first, toRemove[i].second);
		}

		if (containsColorCodesType(str, color))
			checkRemoveColorCode(str, color);
	}

	inline void removeHotkeyColors(std::vector<Ability>& vec, const std::string& color)
	{
		for(auto&& a : vec)
		{
			checkRemoveColorCode(a.tip, color);
			checkRemoveColorCode(a.untip, color);
			checkRemoveColorCode(a.revivetip, color);
			checkRemoveColorCode(a.awakentip, color);
			checkRemoveColorCode(a.researchtip, color);
		}
	}

	inline std::string removeHotkeyColors(const std::string& input, const std::string& color)
	{
		lg::StringIterator iterator(input, "\n\r");

		std::string result;

		while(!iterator.atEnd())
		{
			std::string read = iterator.read();

//			result += checkRemoveColorCode(read, color) + "\n";
		}

		return result;
	}

	inline std::string addMissingTips(const std::string& input, RemoveMask mask,
										const KeyboardLayout& layout)
	{
		std::string result;

		auto list = load(input, false);

		for(auto&& a : list)
		{
			if (!a.tip.size())
			{
				if (!isAbility(a.rawcode))
					a.tip = "Train ";
				a.tip = formatComment(a.name);
			}
		}

		return createString(list, mask, layout);
	}
	/*
	inline void addMissingTips(std::vector<Ability>& vec)
	{
		for(auto&& a : vec)
		{
			if (!a.tip.size())
			{
				if (isAbility(a.rawcode) || isHeroAbility(a.rawcode))
				{
					a.tip = formatComment(a.name);

					if (isHeroAbility(a.rawcode))
					{
						a.researchtip = "Learn " + formatComment(a.name);
					}
				}
				else if (!isHeroAbility(a.rawcode))
				{
					a.tip
				}
				else
				{
				}
			}
		}
	}
	*/
	inline std::string addColorCode(const std::string& input, const std::string& color,
									const std::string& hotkey)
	{
		return "";
	}

	inline std::string addHotkeyColors(const std::string& input, const std::string& color)
	{
		lg::StringIterator iterator(input, "\n\r");

		std::string result;

		while(!iterator.atEnd())
		{
			std::string hotkey;

			std::string read = iterator.read();

			if (isComment(read))
				result += "\n";

			else if (upperCase(read.substr(0, 6)) == upperCase("Hotkey"))
			{
				hotkey = read.substr(7);
			}


			result += addColorCode(read, color, hotkey) + "\n";
		}

		return result;
	}

	typedef std::pair<std::pair<std::string, int>, std::vector<std::string>> FileAbilRet;

	inline FileAbilRet fileAbilitiesCount(std::fstream& file, const std::string& name)
	{
		file.seekg(0, std::fstream::end);
		long long fSize = file.tellg();
		file.seekg(0);

		std::string s;
		s.resize(fSize, ' ');

		file.read(&s[0], s.size());

		lg::StringIterator iter(s, "\n");
		
		FileAbilRet count;

		while(!iter.atEnd())
		{
			std::string read = iter.read();

			if (read.size())
				removeTrailingWhitespaces(read);

			if (read.size())
				removeStartingWhitespaces(read);

			if (isRawcode(read))
			{
				count.second.push_back(parseRawcode(read));
				count.first.second++;
			}
		}

		count.first.first = name;

		return count;
	}

	std::string getMissingList(const std::vector<std::string>& first,
								const std::vector<std::string>& second)
	{
		std::string result;

		//take all entries from first, and compare them to second
		for(auto&& a : first)
		{
			bool found = false;

			for(auto&& b : second)
			{
				if (a == b)
				{
					found = true;
					break;
				}
			}

			if (!found)
				result += a + "\n";
		}

		return result;
	}

	class Benchmark{
		long long startT;
		bool paused;
		long long lastPRet;
	public:
		Benchmark() : startT(__rdtsc()), paused(false), lastPRet(0) {}
		long long pause()
		{
			if (paused)
				return lastPRet;

			paused = true;
			return lastPRet = __rdtsc() - startT;
		}
		long long get()
		{
			return __rdtsc() - startT;
		}

		void start()
		{
			paused = false;
			startT = __rdtsc();
		}
	};
}

inline void parse(ParseData& data)
{
	if (data.compare)
	{
		auto& filePath = data.outFiles;

		auto inFileDat = inner::fileAbilitiesCount(data.inFile, data.inFilePath);
		std::vector<decltype(inFileDat)> outData;

		bool outToFile = data.compareOutput.size();

		std::fstream cOut(data.compareOutput, std::fstream::out | std::fstream::trunc);

		std::fstream debugOut("debugOutput.txt", std::fstream::out | std::fstream::trunc);

		{
			std::string o2 = data.inFilePath;
			int i = 0;
			std::string out;
			for(auto&& a : inFileDat.second)
			{
				out += a + "\n";
				++i;
			}
			o2 += " has " + std::to_string(i) + " abils:\n";

			debugOut << o2 << out << "\n\n";
		}

		for(int i = 0, j = filePath.size(); i < j; ++i)
		{
			std::fstream file(filePath[i], std::fstream::in);
			auto a = inner::fileAbilitiesCount(file, filePath[i]);
			
		{
			std::string o2 = a.first.first;
			int i = 0;
			std::string out;
			for(auto&& a : a.second)
			{
				out += a + "\n";
				++i;
			}
			o2 += " has " + std::to_string(i) + " abils:\n";

			debugOut << o2 << out << "\n\n";
		}

			int diff = a.first.second - inFileDat.first.second;

			//if it is negative, that means that input file has more
			//rawcodes than the output
			if (diff < 0)
			{
				std::string output = a.first.first + " contains " +
									std::to_string(-diff) + " less abilities.\n"+
									"Abilities in " + a.first.first + " which are not in "
									+ inFileDat.first.first + ":\n";

				output += inner::getMissingList(inFileDat.second, a.second);

				//if write to file
				if (outToFile)
				{
					cOut << output << "\n";
				}
				else
				{
					//else just to console
					std::cout << output << "\n";
				}
			}
			else if (diff > 0)
			{
				//somehow, we have more abilities in output than input

				std::string output = a.first.first + " contains " + std::to_string(diff) +
									" more abilities.\nAbilities in " + inFileDat.first.first +
									" which are not in " + a.first.first + ":\n";

				output += inner::getMissingList(a.second, inFileDat.second);

				if (outToFile)
					cOut << output << "\n";
				else
					std::cout << output << "\n";
			}
		}
	}

	else if (data.showTimeOutput)
	{
		auto formatTime = [](long long a, long long cpuClock)
							{
								return static_cast<double>(a) / cpuClock;
							};

		inner::Benchmark bMrk;
		auto fullStart = bMrk.get();
		auto current = bMrk.get();
		auto cpuClock = data.cpuClock ? data.cpuClock : inner::cpuclock();

		auto loaded = inner::load(data.inFile, data.debugMode);
	//	auto output = inner::createString(loaded, data.mask);

		std::cout << "\tLoading from file took " << formatTime(bMrk.get() - current, cpuClock)
					<< " seconds\n";

		if (data.hkcolor.size())
		{
			current = bMrk.get();

			inner::removeHotkeyColors(loaded, data.hkcolor);
			
			std::cout << "\tRemoving hotkey colors took " << formatTime(bMrk.get() - current, cpuClock)
						<< " seconds\n";
		}

		//output = inner::addMissingTips(output, data.mask);

	//	if (data.color.size())
	//		output = inner::addHotkeyColors(output, data.color);

		current = bMrk.get();

		auto output = inner::createString(loaded, data.mask, data.layout);

		std::cout << "\tCreating string from data took " << formatTime(bMrk.get() - current, cpuClock)
				<< " seconds\n";

		current = bMrk.get();

		inner::writeResult(data, output);

		std::cout << "\tWriting to file took " << formatTime(bMrk.get() - current, cpuClock)
					<< " seconds.\n";

		std::cout << "\tWhole operation took " << formatTime(bMrk.get() - fullStart, cpuClock)
					<< " seconds.\n";

		std::cout << "\tCPU Clock used: " << cpuClock << "\n";
		std::cout << "\nIf you are unsatisfied with this, you can set your cpu clock manually";
		std::cout<< "With -cpuclock=NUMBER\n"
					"with optional suffix Ghz, Mhz or Khz(no Hz if pure number)\n";
	}
	else
	{
		auto loaded = inner::load(data.inFile, data.debugMode);
	//	auto output = inner::createString(loaded, data.mask);

		if (data.hkcolor.size())
			inner::removeHotkeyColors(loaded, data.hkcolor);

		//output = inner::addMissingTips(output, data.mask);

	//	if (data.color.size())
	//		output = inner::addHotkeyColors(output, data.color);
		auto output = inner::createString(loaded, data.mask, data.layout);

		inner::writeResult(data, output);
	}
}

#endif //_PARSER_H_